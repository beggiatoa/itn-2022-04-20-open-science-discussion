# Open Science Presentation 2022-04-20
## Description
### Purpose

Moderate a (short) discussion about open science and citizen science during the
ITN Workshop (Galway, Ireland. March 2022, 19th -- 22nd).

### Principles

The slides should be complete rather than minimal and attractive.  The slides
should point to various references and additional resources that can be used
after the discussion.

### Vision

Use a simple **two slides** presentation to introduce and spark questions and
discussion around open science (pre-registration) and citizen science (water
consumption).

## Requirements

- LibreOffice Impress

## Authors

- Marco Prevedello <m.prevedello1@nuigalway.ie> [@Preve92](https://twitter.com/Preve92)

## License

- All original text and accompanying media under the terms of the Creative
Commons CC0 1.0 Universal.

## Acknowledgments

Thanks to Raffaello Mattiussi and Kris A. Silveira for letting me participate in
this event.
